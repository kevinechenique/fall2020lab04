//Kevin Echenique Arroyo #1441258

package geometry;

public class Rectangle implements Shape{
	
	private double width;
	private double length;
	private double area;
	private double perimeter;
	
	//CONSTRUCTOR
	public Rectangle(double width, double length) {
		this.width = width;
		this.length = length;
	}
	
	//GET METHODS
	public double getWidth() {
		return this.width;
	}
	public double getLength() {
		return this.length;
	}
	
	public double getArea() {
		double area = this.width*this.length;
		this.area = area;
		return this.area;
	}
	public double getPerimeter() {
		double perimeter = (2*this.width)+(2*this.length);
		this.perimeter = perimeter;
		return this.perimeter;
	}

}
