//Kevin Echenique Arroyo #1441258

package geometry;

public class Circle implements Shape{
	
	private double area;
	private double perimeter;
	private double radius;
	
	//CONSTRUCTOR
	public Circle(double radius) {
		this.radius = radius;
	}
	
	//gets radius of the circle
	public double getRadius() {
		return this.radius;
	}
	
	//gets the area of the circle
	public double getArea() {
		//area is PI*r^2
		double area = Math.PI*Math.pow(this.radius, 2);
		this.area = area;
		return this.area;
	}
	
	//gets the circumference of the circle
	public double getPerimeter() {
		//circumference is 2*PI*r
		double perimeter = 2*Math.PI*this.radius;
		this.perimeter = perimeter;
		return this.perimeter;
	}

}
