//Kevin Echenique Arroyo #1441258

package geometry;

/**This class extends the rectangle class since a square is a rectangle and Rectangle class implements the Shape interface.
 * Since the Rectangle class implements the Shape interface, The Square class automatically implements the Shape interface.
 * Since Square inherit the methods from the Rectangle class, they automatically meet all the requirements for the Shape interface.
 */
public class Square extends Rectangle{
	
	//We do not add any additional fields since we are extending the Ractangle class. We automatically have a width and a length
	public Square(double sideLength) {
		
		//here we call the rectangle constructor and add the measures of the sides of the square. We should get the area and perimeter without adding more methods or overriding the area or perimeter methods of the Rectangle class
		super(sideLength, sideLength);
		
	}
}
