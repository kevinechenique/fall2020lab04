//Kevin Echenique Arroyo #1441258

package geometry;

public class LotsOfShapes {
	
	public static void main(String[] args) {
		
		Shape[] figures = new Shape[5];
		
		/*Cannot create an object of type shape since shape is an interface.
		 * You can create an array of Shape because it simply is storing objects in it
		
		figures[0] = new Shape;*/
		
		//Creating two Rectangles
		Shape rec1 = new Rectangle(2, 3);
		Shape rec2 = new Rectangle(4, 6);
		//Creating two Circles
		Shape cir1 = new Circle(2);
		Shape cir2 = new Circle(5);
		//Creating one square
		Shape square = new Square(4);
		
		figures[0] = rec1;
		figures[1] = rec2;
		figures[2] = cir1;
		figures[3] = cir2;
		figures[4] = square;
		
		//Printing all figures' area and perimeter
		for(Shape s : figures) {
			System.out.println("Area: " + s.getArea());
			System.out.println("Perimeter: " + s.getPerimeter());
		}
		
	}

}
