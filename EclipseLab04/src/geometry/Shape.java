//Kevin Echenique Arroyo #1441258

package geometry;

//This is not a class. It is an interface
public interface Shape {
	
	/*since this is not a class we do NOT write public before the method names and no arguments given inside the method.
	 * Yup! This is a method, even without the { } since no code will go inside of it in this interface.
	 * These methods will be used by subclasses instead.
	 */
	double getArea();//This method returns the are of a given shape
	
	double getPerimeter();//This method return the perimeter of a given shape
	

}
