//Kevin Echenique Arroyo #1441258

package inheritance;
import geometry.*;

public class Test {

	public static void main(String[] args) {
		
		//BOTH CASES WORK
		
		/*ElectronicBook eb = new ElectronicBook("Pain", "Chloe", 100000);
		System.out.println(eb);*/
		
		/*Book b = new ElectronicBook("Relieve", "Lia", 1500000);
		System.out.println(b);*/
		
		/*Shape circle = new Circle(35);
		System.out.println(circle.getArea());*/
		
		//Using the interface Shape with the Square class even though Square only inherits from Rectangle, but rectangle implements to Shape
		Shape square = new Square(5);
		System.out.println(square.getPerimeter());

	}

}
