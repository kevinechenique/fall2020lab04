//Kevin Echenique Arroyo #1441258

package inheritance;

public class BookStore {

	public static void main(String[] args) {
		
		//Making 5 books, they could be electronic or hard copy
		Book[] books = new Book[5];
		
		/**creating individual books to store in our array of Books
		 * 1st & 3rd position: are hard copy books (Book)
		 * 2nd, 4th & 5th position: are electronic books (ElectronicBook) 
		 */
		Book b1 = new Book("Little Pig", "Harry Swine");
		Book b2 = new Book("Top 5 lullabies", "Chloe Sleepy");
		Book el1 = new ElectronicBook("Nothing to see", "Ruth Blindman", 30000000);
		Book el2 = new ElectronicBook("You and I", "Mia lone", 18000000);
		Book el3 = new ElectronicBook("Be Happy Today", "Ben Liar", 25000000);
		
		books[0] = b1;
		books[1] = el1;
		books[2] = b2;
		books[3] = el2;
		books[4] = el3;
		
		for(Book b : books) {
			System.out.println(b);
		}
		

	}

}
