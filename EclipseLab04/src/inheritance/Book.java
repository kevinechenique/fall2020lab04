//Kevin Echenique Arroyo #1441258

package inheritance;

public class Book {//This is called the super class. Other subclasses will inherit from this class
	
	protected String bookTitle;
	private String bookAuthor;
	
	//CONSTRUCTOR
	public Book(String bookTitle, String bookAuthor) {
		this.bookTitle = bookTitle;
		this.bookAuthor = bookAuthor;
	}
	
	//GET METHODS
	public String getBookTitle() {
		return this.bookTitle;
	}
	public String getBookAuthor() {
		return this.bookAuthor;
	}
	
	//toString METHOD
	public String toString() {
		return("Title: "+this.bookTitle+"\n"+"Author: "+this.bookAuthor);
	}
	

}
