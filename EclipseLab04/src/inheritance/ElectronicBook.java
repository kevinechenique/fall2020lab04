//Kevin Echenique Arroyo #1441258

package inheritance;

public class ElectronicBook extends Book {//This is a subclass that inherits from the super class Book
	
	private int numberBytes;
	
	//CONSTRUCTOR
	public ElectronicBook(String bookTitle, String bookAuthor, int numberBytes) {
		/**Here we are calling the constructor from the superclass Book.
		 * This way of calling the constructor from the superclass is called explicit
		 * because we are specifying (or setting) its input. This other way is implicit
		 * and it happens when we call the constructor from the superclass with no input
		 * (e.g., super(); the super keywords has no input in it. 
		 */
		super(bookTitle, bookAuthor);
		this.numberBytes = numberBytes;
	}
	
	public String toString() {
		String fromBase = super.toString();//We are concatenating the toString from the Book superclass to the ElectronicBook ToString  
		return(fromBase+"\n"+"Size: "+this.numberBytes+" Bytes");
	}
}
